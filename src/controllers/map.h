#ifndef ASTRUM_CONTROLLERS_MAP_H
#define ASTRUM_CONTROLLERS_MAP_H

#include <ludo/api.h>

namespace astrum
{
  void control_map(ludo::instance& inst);
}

#endif // ASTRUM_CONTROLLERS_MAP_H
