#ifndef ASTRUM_CONTROLLERS_PERSON_H
#define ASTRUM_CONTROLLERS_PERSON_H

#include <ludo/api.h>

namespace astrum
{
  void control_person(ludo::instance& inst, uint32_t index);
}

#endif // ASTRUM_CONTROLLERS_PERSON_H
