#ifndef ASTRUM_CONTROLLERS_SPACESHIP_H
#define ASTRUM_CONTROLLERS_SPACESHIP_H

#include <ludo/api.h>

namespace astrum
{
  void control_spaceship(ludo::instance& inst, uint32_t index);
}

#endif // ASTRUM_CONTROLLERS_SPACESHIP_H
