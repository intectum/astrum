#ifndef ASTRUM_CONTROLLERS_GAME_H
#define ASTRUM_CONTROLLERS_GAME_H

#include <ludo/api.h>

namespace astrum
{
  void control_game(ludo::instance& inst);
}

#endif // ASTRUM_CONTROLLERS_GAME_H
