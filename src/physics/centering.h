#ifndef ASTRUM_PHYSICS_CENTERING_H
#define ASTRUM_PHYSICS_CENTERING_H

#include <ludo/api.h>

namespace astrum
{
  void center_universe(ludo::instance& inst);
}

#endif // ASTRUM_PHYSICS_CENTERING_H
