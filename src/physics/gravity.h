#ifndef ASTRUM_PHYSICS_GRAVITY_H
#define ASTRUM_PHYSICS_GRAVITY_H

#include <ludo/api.h>

namespace astrum
{
  void simulate_gravity(ludo::instance& inst);
}

#endif // ASTRUM_PHYSICS_GRAVITY_H
