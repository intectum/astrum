#ifndef ASTRUM_SOLAR_SYSTEM_H
#define ASTRUM_SOLAR_SYSTEM_H

#include <ludo/api.h>

namespace astrum
{
  void add_solar_system(ludo::instance& inst);
}

#endif // ASTRUM_SOLAR_SYSTEM_H
