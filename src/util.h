#ifndef ASTRUM_UTIL_H
#define ASTRUM_UTIL_H

#include <ludo/api.h>

namespace astrum
{
  void print_timings(ludo::instance& inst);
}

#endif // ASTRUM_UTIL_H
